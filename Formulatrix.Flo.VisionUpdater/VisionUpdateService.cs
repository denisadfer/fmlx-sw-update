﻿using Formulatrix.Flo.Service.JsonDiffPatchs;
using Formulatrix.Flo.Service.Shell;
using Formulatrix.Flo.VisionUpdater.ConfigDefs;
using Microsoft.Extensions.Logging;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Formulatrix.Flo.VisionUpdater
{
  public class VisionUpdateService
  {
    private readonly FloVisionUpdateConfig _config;

    private readonly SftpService _sftpService;
    private readonly SshService _sshService;

    private readonly JsonDiffPatchService _jsonDiffPatchService;

    private readonly ILogger _logger;

    public VisionUpdateService( FloVisionUpdateConfig config, IService[] services, JsonDiffPatchService jsonDiffPatchService, ILogger logger )
    {
      _logger = logger;

      _config = config;

      _sftpService = (SftpService)services.First( x => x is SftpService );
      _sshService = (SshService)services.First( x => x is SshService );

      _jsonDiffPatchService = jsonDiffPatchService;
    }

    public ErrorCode DoUpdate( string currentVersion )
    {
      try
      {
        //Read update version
        var updateVersion = File.ReadAllText( _config.MainProcessor.FileVersionPath );
        var currentVersionSplit = currentVersion.Split( '.' );
        var updateVersionSplit = updateVersion.Split( '.' );
        if( currentVersionSplit.Length != updateVersionSplit.Length )
        {
          return ErrorCode.InvalidFile;
        }
        
        //Compare version
        if( updateVersionSplit.Where( ( t, i ) =>
             int.Parse( t, CultureInfo.InvariantCulture ) <=
             int.Parse( currentVersionSplit[i], CultureInfo.InvariantCulture ) ).Any() )
        {
          return ErrorCode.NoError;
        }
      }
      catch( FileNotFoundException )
      {
        return ErrorCode.FileNotFound;
      }

      CheckConnection( _sshService );

      //Backup binary
      _logger.LogDebug( "DoUpdate,Backup,start" );
      var errorCode = FileManager.CleanDirectory( _sshService, _config.SubProcessor.BackupPath, out _ );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,Backup,end,errorCode={errorCode}" );
        return  errorCode;
      }
      
      errorCode = FileManager.CopyDirectory( _sshService, 
                                                 _config.SubProcessor.BinaryPath, 
                                                 _config.SubProcessor.BackupPath, 
                                                 out _ );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,Backup,end,errorCode={errorCode}" );
        return  errorCode;
      }

      //Download config
      _logger.LogDebug( "DoUpdate,Download config,start" );
      errorCode = FileManager.DownloadFile( _sftpService, 
                                            _config.SubProcessor.ConfigFilePath, 
                                            _config.MainProcessor.ConfigFileDownloadPath );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,Download config,end,errorCode={errorCode}" );
        return  errorCode;
      }
      
      //Download default config
      _logger.LogDebug( "DoUpdate,Download default config,start" );
      errorCode = FileManager.DownloadFile( _sftpService, 
                                            _config.SubProcessor.DefaultConfigFilePath, 
                                            _config.MainProcessor.DefaultConfigFileDownloadPath );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,Download default config,end,errorCode={errorCode}" );
        return  errorCode;
      }
      
      //Create patch
      _logger.LogDebug( "DoUpdate,Create patch,start" );
      errorCode = _jsonDiffPatchService.GeneratePatch( _config.MainProcessor.DefaultConfigFileDownloadPath, _config.MainProcessor.ConfigFileUpdatePath, out string jsonPatch );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,Create patch,end,errorCode={errorCode}" );
        return errorCode;
      }
      
      //Patch download config
      _logger.LogDebug( "DoUpdate,Apply patch,start" );
      errorCode = _jsonDiffPatchService.ApplyPatch( jsonPatch, _config.MainProcessor.ConfigFileDownloadPath );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,Apply patch,end,errorCode={errorCode}" );
        return errorCode;
      }
      
      //Clean default config directory
      _logger.LogDebug( "DoUpdate,Clean default config,start" );
      errorCode = FileManager.CleanDirectory( _sshService, _config.SubProcessor.DefaultConfigFilePath, out _ );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,Clean default config,end,errorCode={errorCode}" );
        return  errorCode;
      }
      
      //Update default config
      _logger.LogDebug( "DoUpdate,Upload default config,start" );
      errorCode = FileManager.UploadFile( _sftpService, 
                                          _config.MainProcessor.ConfigFileUpdatePath, 
                                          _config.SubProcessor.DefaultConfigFilePath );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,Upload default config,end,errorCode={errorCode}" );
        return  errorCode;
      }
      
      //Clean config directory
      _logger.LogDebug( "DoUpdate,Clean config,start" );
      errorCode = FileManager.CleanDirectory( _sshService, _config.SubProcessor.ConfigFilePath, out _ );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,Clean config,end,errorCode={errorCode}" );
        return  errorCode;
      }
      
      //Update config
      _logger.LogDebug( "DoUpdate,Upload config,start" );
      errorCode = FileManager.UploadFile( _sftpService, 
                                          _config.MainProcessor.ConfigFileDownloadPath, 
                                          _config.SubProcessor.ConfigFilePath );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,Upload config,end,errorCode={errorCode}" );
        return  errorCode;
      }
      
      //Stop service
      _logger.LogDebug( "DoUpdate,Stop service,start" );
      errorCode = FileManager.StopService( _sshService, _config.SubProcessor.ServiceName, out _ );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,Stop service,end,errorCode={errorCode}" );
        return  errorCode;
      }
      
      //Upload binary
      _logger.LogDebug( "DoUpdate,Upload binary,start" );
      errorCode = FileManager.UploadFile( _sftpService, 
                                          _config.MainProcessor.FileUpdatePath, 
                                          _config.SubProcessor.BinaryPath, 
                                          null, 
                                          FileManager.NOT_EQUAL, 
                                          new[] { "config" } );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,Upload binary,end,errorCode={errorCode}" );
        return  errorCode;
      }
      
      //Set permission
      _logger.LogDebug( "DoUpdate,Set permission,start" );
      errorCode = FileManager.SetPermission( _sftpService, _config.SubProcessor.ApplicationPath, 775 );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,Set permission,end,errorCode={errorCode}" );
        return  errorCode;
      }
      
      //Start service
      _logger.LogDebug( "DoUpdate,Start service,start" );
      errorCode = FileManager.StartService( _sshService, _config.SubProcessor.ServiceName, out _ );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,Start service,end,errorCode={errorCode}" );
        return  errorCode;
      }

      _logger.LogDebug( $"DoUpdate,end,errorCode={errorCode}" );
      return ErrorCode.NoError;
    }

    private void CheckConnection( IService sshService )
    {
      while( true )
      {
        _logger.LogDebug( "CheckConnection,start" );
        var errorCode = sshService.Connect();
        if( errorCode != ErrorCode.NoError )
        {
          _logger.LogError( $"CheckConnection,end,errorCode={errorCode},retry" );
          continue;
        }
        sshService.Disconnect();
        _logger.LogDebug( $"CheckConnection,end,errorCode={errorCode}" );
        break;
      }
    }
  }
}

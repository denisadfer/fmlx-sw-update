using Newtonsoft.Json;

namespace Formulatrix.Flo.VisionUpdater.ConfigDefs
{
  public class VisionServiceConfig
  {
    [JsonProperty( Required = Required.Always )]
    public string IpAddress { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    public string Username { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    public string Password { get; set; }
  }
}

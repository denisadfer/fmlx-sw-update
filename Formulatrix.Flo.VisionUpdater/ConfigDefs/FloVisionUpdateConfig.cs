using Newtonsoft.Json;

namespace Formulatrix.Flo.VisionUpdater.ConfigDefs
{
  public class FloVisionUpdateConfig
  {
    public MainProcessor MainProcessor { get; set; }
    
    public SubProcessor SubProcessor { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    public bool SimMode { get; set; }
  }

  public class MainProcessor
  {
    [JsonProperty( Required = Required.Always )]
    public string FileVersionPath { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    public string FileUpdatePath { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    public string ConfigFileUpdatePath { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    public string ConfigFileDownloadPath { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    public string DefaultConfigFileDownloadPath { get; set; }
  }

  public class SubProcessor
  {
    [JsonProperty( Required = Required.Always )]
    public string BinaryPath { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    public string ConfigFilePath { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    public string DefaultConfigFilePath { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    public string BackupPath { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    public string ApplicationPath { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    public string ServiceName { get; set; }
  }
}

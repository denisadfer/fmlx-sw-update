﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Formulatrix.Flo.Service.Shell;
using Microsoft.Extensions.Logging;
using Mono.Unix.Native;

namespace Formulatrix.Flo
{
  public static class FileManager
  {
    public static ErrorCode CopyFile( string sourcePath, string destinationPath, string filename,
      ILogger logger = null, bool overwrite = true )
    {
      logger?.LogInformation( $"CopyFile,sourcePath={sourcePath},destination={destinationPath},filename={filename}" );
      try
      {
        FileInfo info = new FileInfo( Path.Combine( sourcePath, filename ) );
        string fileDestination = Path.Combine( destinationPath, filename );
        if( !info.Exists )
          return ErrorCode.FileNotFound;
        
        info.CopyTo( fileDestination , overwrite );
      }
      catch( Exception e )
      {
        logger?.LogError( $"CopyFile,exception,type={e.GetType().FullName},message={e.StackTrace}" );
        throw;
      }
      
      return ErrorCode.NoError;
    }
    
    public static ErrorCode CleanDirectory( string path )
    {
      if( Directory.Exists( path ) )
      {
        DirectoryInfo directory = new DirectoryInfo( path );
        foreach( FileInfo file in directory.EnumerateFiles() )
        {
          file.Delete();
        }
        foreach( DirectoryInfo dir in directory.GetDirectories() )
        {
          Directory.Delete( dir.FullName, true );
        }
      }
      else
      {
        Directory.CreateDirectory( path );
      }
      return ErrorCode.NoError;
    }

    public static ErrorCode CleanDirectory( SshService sshService, string path, out string result )
    {
      result = string.Empty;
      var errorCode = sshService.Connect();
      if( errorCode == ErrorCode.NoError )
      {
        errorCode = sshService.SendCommand( $"rm -f -r {path}/*", out result );
      }
      else
      {
        return errorCode;
      }
      sshService.Disconnect();
      return errorCode;
    }

    public const string EQUAL = "==";
    public const string NOT_EQUAL = "!=";

    public static ErrorCode CopyDirectory( string sourcePath, string destinationPath, bool recursive, string prefixPattern = null, string[] searchPattern = null, bool overwrite = true, ILogger logger = null )
    {
      DirectoryInfo directory = new DirectoryInfo( sourcePath );

      if( !directory.Exists )
      {
        return ErrorCode.DirectoryNotFound;
      }

      try
      {
        DirectoryInfo[] directories;
        if( prefixPattern == null )
        {
          directories = directory.GetDirectories();
        }
        else
        {
          if( prefixPattern == EQUAL )
          {
            directories = directory.GetDirectories().Where( x => ( searchPattern ?? Array.Empty<string>() ).Contains( x.Name ) ).ToArray();
          }
          else
          {
            directories = directory.GetDirectories().Where( x => !( searchPattern ?? Array.Empty<string>() ).Contains( x.Name ) ).ToArray();
          }
        }

        Directory.CreateDirectory( destinationPath );

        foreach( FileInfo file in directory.EnumerateFiles() )
        {
          string tempPath = Path.Combine( destinationPath, file.Name );
          logger?.LogDebug( $"CopyDirectory,file={tempPath}" );
          file.CopyTo( tempPath, overwrite );
        }

        if( recursive )
        {
          foreach( DirectoryInfo subDirectory in directories )
          {
            string tempPath = Path.Combine( destinationPath, subDirectory.Name );
            CopyDirectory( subDirectory.FullName, tempPath, true, null, null, true, logger );
          }
        }
      }
      catch( Exception e )
      {
        logger?.LogError( $"CopyDirectory,exception,type={e.GetType().FullName},message={e.StackTrace}" );
        throw;
      }
      
      return ErrorCode.NoError;
    }

    public static ErrorCode CopyDirectory( SshService sshService, string sourcePath, string destinationPath, out string result )
    {
      result = string.Empty;
      var errorCode = sshService.Connect();
      if( errorCode == ErrorCode.NoError )
      {
        errorCode = sshService.SendCommand( $"cp -r {sourcePath}/* {destinationPath}", out result );
      }
      else
      {
        return errorCode;
      }
      sshService.Disconnect();
      return errorCode;
    }

    public static ErrorCode DownloadFile( SftpService sftpService, string sourcePath, string destinationPath, Action<ulong> callback = null, bool recursive = true )
    {
      var errorCode = ErrorCode.NoError;

      if( !sftpService.IsConnected() )
      {
        errorCode = sftpService.Connect();
      }

      if( errorCode == ErrorCode.NoError )
      {
        errorCode = sftpService.ListDirectory( sourcePath, out var files );
        if( errorCode == ErrorCode.NoError )
        {
          foreach( var file in files )
          {
            if( !file.IsDirectory && !file.IsSymbolicLink )
            {
              if( !Directory.Exists( destinationPath ) )
              {
                Directory.CreateDirectory( destinationPath );
              }
              var destination = Path.Combine( destinationPath, file.Name );
              using( var fileStream = new FileStream( destination, FileMode.Create ) )
              {
                errorCode = sftpService.DownloadFile( file.FullName, fileStream, callback );
              }
            }
            else if( file.Name != "." && file.Name != ".." )
            {
              var directory = Directory.CreateDirectory( Path.Combine( destinationPath, file.Name ) );
              if( recursive )
              {
                DownloadFile( sftpService, file.FullName, directory.FullName, callback );
              }
            }
          }
        }
      }
      else
      {
        return errorCode;
      }

      if( sftpService.IsConnected() )
      {
        sftpService.Disconnect();
      }
      return errorCode;
    }

    public static ErrorCode UploadFile( SftpService sftpService, string sourcePath, string destinationPath, Action<ulong> callback = null, string prefixPattern = null, string[] searchPattern = null, bool recursive = true )
    {
      var errorCode = ErrorCode.NoError;

      if( !sftpService.IsConnected() )
      {
        errorCode = sftpService.Connect();
      }

      if( errorCode == ErrorCode.NoError )
      {
        if( !sftpService.IsDirectoryExist( destinationPath ) )
        {
          sftpService.CreateDirectory( destinationPath );
        }

        var directory = new DirectoryInfo( sourcePath );
        foreach( var file in directory.EnumerateFiles() )
        {
          using( var fileStream = new FileStream( file.FullName, FileMode.Open, FileAccess.Read ) )
          {
            var newDestinationPath = Path.Combine( destinationPath, file.Name ).Replace( @"\", @"/" );
            errorCode = sftpService.UploadFile( fileStream, newDestinationPath, callback );
          }
        }

        if( recursive )
        {
          DirectoryInfo[] directories;
          if( prefixPattern == null )
          {
            directories = directory.GetDirectories();
          }
          else
          {
            if( prefixPattern == EQUAL )
            {
              directories = directory.GetDirectories().Where( x => ( searchPattern ?? Array.Empty<string>() ).Contains( x.Name ) ).ToArray();
            }
            else
            {
              directories = directory.GetDirectories().Where( x => !( searchPattern ?? Array.Empty<string>() ).Contains( x.Name ) ).ToArray();
            }
          }
          foreach( var subDirectory in directories )
          {
            var newDestinationPath = Path.Combine( destinationPath, subDirectory.Name ).Replace( @"\", @"/" );
            UploadFile( sftpService, subDirectory.FullName, newDestinationPath, callback );
          }
        }
      }
      else
      {
        return errorCode;
      }


      if( sftpService.IsConnected() )
      {
        sftpService.Disconnect();
      }
      return errorCode;
    }

    public const FilePermissions PERMISSION775 = FilePermissions.S_IRWXU |
                                                 FilePermissions.S_IRGRP |
                                                 FilePermissions.S_IXGRP |
                                                 FilePermissions.S_IROTH |
                                                 FilePermissions.S_IXOTH;

    public static ErrorCode SetPermission( string path, FilePermissions permissions )
    {
      var result = Syscall.chmod( path, permissions );
      if( result != 0 )
      {
        return ErrorCode.SetPermissionFailed;
      }
      return ErrorCode.NoError;
    }

    public static ErrorCode SetPermission( SftpService sftpService, string path, short permission )
    {
      var errorCode = sftpService.Connect();
      if( errorCode == ErrorCode.NoError )
      {
        errorCode = sftpService.SetPermission( path, permission );
      }
      else
      {
        return errorCode;
      }
      sftpService.Disconnect();
      return errorCode;
    }

    public static ErrorCode StopService( string serviceName )
    {
      ErrorCode errorCode = ErrorCode.NoError;
      try
      {
        var startInfo = new ProcessStartInfo()
        {
          FileName = "/bin/bash", Arguments = $"-c \"systemctl stop {serviceName}\"", CreateNoWindow = true
        };
        var process = new Process()
        {
          StartInfo = startInfo,
        };
        process.Start();
        process.WaitForExit();
      }
      catch( InvalidOperationException )
      {
        errorCode = ErrorCode.ServiceNotExists;
        return errorCode;
      }

      return errorCode;
    }

    public static ErrorCode StopService( SshService sshService, string serviceName, out string result )
    {
      result = string.Empty;
      var errorCode = sshService.Connect();
      if( errorCode == ErrorCode.NoError )
      {
        errorCode = sshService.SendCommand( $"sudo systemctl stop {serviceName}", out result );
      }
      else
      {
        return errorCode;
      }
      sshService.Disconnect();
      return errorCode;
    }

    public static ErrorCode StartService( string serviceName )
    {
      ErrorCode errorCode = ErrorCode.NoError;
      try
      {
        var startInfo = new ProcessStartInfo()
        {
          FileName = "/bin/bash", Arguments = $"-c \"systemctl start {serviceName}\"", CreateNoWindow = true
        };
        var process = new Process()
        {
          StartInfo = startInfo,
        };
        process.Start();
      }
      catch( InvalidOperationException )
      {
        errorCode = ErrorCode.ServiceNotExists;
        return errorCode;
      }

      return errorCode;
    }

    public static ErrorCode StartService( SshService sshService, string serviceName, out string result )
    {
      result = string.Empty;
      var errorCode = sshService.Connect();
      if( errorCode == ErrorCode.NoError )
      {
        errorCode = sshService.SendCommand( $"sudo systemctl start {serviceName}", out result );
      }
      else
      {
        return errorCode;
      }
      sshService.Disconnect();
      return errorCode;
    }
  }
}

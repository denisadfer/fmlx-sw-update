﻿using Microsoft.Extensions.Logging;

namespace Formulatrix.Flo
{
  public class DefaultLogger
  {
    private static ILogger _logger;

    /// <summary>
    /// Assign a logger to be used as default logger
    /// </summary>
    /// <param name="logger"></param>
    public DefaultLogger Build( ILogger logger )
    {
      _logger = logger;
      return this;
    }

    public static void LogTrace( string message, params object[] args )
    {
      _logger.LogTrace( message, args );
    }

    public static void LogDebug( string message, params object[] args )
    {
      _logger.LogDebug( message, args );
    }

    public static void LogInformation( string message, params object[] args )
    {
      _logger.LogInformation( message, args );
    }

    public static void LogWarning( string message, params object[] args )
    {
      _logger.LogWarning( message, args );
    }

    public static void LogError( string message, params object[] args )
    {
      _logger.LogError( message, args );
    }

    public static void LogCritical( string message, params object[] args )
    {
      _logger.LogCritical( message, args );
    }
  }
}

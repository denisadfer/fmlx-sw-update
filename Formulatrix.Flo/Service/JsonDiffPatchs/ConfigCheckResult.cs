﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Formulatrix.Flo.Service.JsonDiffPatchs
{
  public class ConfigCheckResult
  {
    public List<Tuple<string, JToken>> AddedFiles { get; }
    public List<Tuple<string, JToken>> RemovedFiles { get; }
    public List<Tuple<string, JToken>> ChangedFiles { get; }

    public ConfigCheckResult( List<Tuple<string, JToken>> addedFiles, List<Tuple<string, JToken>> removedFiles, List<Tuple<string, JToken>> changedFiles )
    {
      AddedFiles = addedFiles;
      RemovedFiles = removedFiles;
      ChangedFiles = changedFiles;
    }
  }
}

using JsonDiffPatchDotNet;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Formulatrix.Flo.Service.JsonDiffPatchs
{
  public class JsonDiffPatchService
  {
    private readonly ConfigFileLoader _configFileLoader;
    private readonly JsonDiffPatch _jsonDiffPatch;

    private readonly ILogger _logger;

    public JsonDiffPatchService( ILogger logger )
    {
      _logger = logger;

      _configFileLoader = new ConfigFileLoader();
      _jsonDiffPatch = new JsonDiffPatch();
    }

    public ErrorCode GeneratePatch( string leftDirectoryPath, string rightDirectoryPath, out string jsonPatch )
    {
      _logger.LogInformation($"GeneratePatch, leftDirectoryPath={leftDirectoryPath}, rightDirectoryPath {rightDirectoryPath}");
      jsonPatch = string.Empty;

      ErrorCode errorCode = _configFileLoader.LoadFiles( leftDirectoryPath, out List<Tuple<string, JToken>> leftFileList );
      if( errorCode != ErrorCode.NoError )
      {
        return errorCode;
      }

      errorCode = _configFileLoader.LoadFiles( rightDirectoryPath, out List<Tuple<string, JToken>> rightFileList );
      if( errorCode != ErrorCode.NoError )
      {
        return errorCode;
      }

      ConfigCheckResult result = CheckFiles( leftFileList, rightFileList );
      ConfigFilePatch patches = CreatePatch( result );

      jsonPatch = JsonConvert.SerializeObject( patches, Formatting.Indented );
      _logger.LogDebug( jsonPatch );
      return ErrorCode.NoError;
    }

    public ErrorCode ApplyPatch( string jsonPatch, string outputDirectory )
    {
      _logger.LogInformation($"ApplyPatch, jsonPatch={jsonPatch}, outputDirectory={outputDirectory}");
      ConfigFilePatch patch = JsonConvert.DeserializeObject<ConfigFilePatch>( jsonPatch );

      ErrorCode errorCode = _configFileLoader.LoadFiles( outputDirectory, out List<Tuple<string, JToken>> outputFileList );
      if( errorCode != ErrorCode.NoError )
      {
        return errorCode;
      }

      foreach( ConfigFilePatchDetail patchDetail in patch.ConfigFilePatches )
      {
        int index;
        string path;

        switch( patchDetail.Action )
        {
          case PatchAction.Add:
            try
            {
              path = $"{outputDirectory}{patchDetail.Filename}";
              var dir = Path.GetDirectoryName( path );

              if( Directory.Exists( dir ) )
              {
                File.WriteAllText( path, JsonConvert.SerializeObject( patchDetail.Content, Formatting.Indented ) );
              }
              else
              {
                Directory.CreateDirectory( dir );
                File.WriteAllText( path, JsonConvert.SerializeObject( patchDetail.Content, Formatting.Indented ) );
              }
            }
            catch( Exception ex )
            {
              _logger.LogError( ex.StackTrace );
              errorCode = ErrorCode.ModifyingDataFailed;
            }
            break;
          case PatchAction.Change:
            try
            {
              index = outputFileList.FindIndex( x => x.Item1.Equals( patchDetail.Filename ) );
              if( index != -1 )
              {
                JToken newContent = _jsonDiffPatch.Patch( outputFileList[index].Item2, patchDetail.Content );
                path = $"{outputDirectory}{patchDetail.Filename}";
                File.WriteAllText( path, JsonConvert.SerializeObject( newContent, Formatting.Indented ) );
              }
            }
            catch( Exception ex )
            {
              _logger.LogError( ex.StackTrace );
              errorCode = ErrorCode.ModifyingDataFailed;
            }
            break;
          case PatchAction.Remove:
            try
            {
              path = $"{outputDirectory}{patchDetail.Filename}";
              if( File.Exists( path ) )
              {
                File.Delete( path );
              }
            }
            catch( Exception ex )
            {
              _logger.LogError( ex.StackTrace );
              errorCode = ErrorCode.ModifyingDataFailed;
            }
            break;
        }
      }

      return errorCode;
    }

    private ConfigFilePatch CreatePatch( ConfigCheckResult result )
    {
      List<ConfigFilePatchDetail> patches = new List<ConfigFilePatchDetail>();

      foreach( var patch in result.AddedFiles )
      {
        patches.Add( new ConfigFilePatchDetail( PatchAction.Add, patch.Item1, patch.Item2 ) );
      }

      foreach( var patch in result.ChangedFiles )
      {
        patches.Add( new ConfigFilePatchDetail( PatchAction.Change, patch.Item1, patch.Item2 ) );
      }

      foreach( var patch in result.RemovedFiles )
      {
        patches.Add( new ConfigFilePatchDetail( PatchAction.Remove, patch.Item1, patch.Item2 ) );
      }

      return new ConfigFilePatch( patches );
    }

    private ConfigCheckResult CheckFiles( List<Tuple<string, JToken>> leftFileList, List<Tuple<string, JToken>> rightFileList )
    {
      List<Tuple<string, JToken>> addedFiles = new List<Tuple<string, JToken>>();
      List<Tuple<string, JToken>> removedFiles = new List<Tuple<string, JToken>>();
      List<Tuple<string, JToken>> changedFiles = new List<Tuple<string, JToken>>();

      foreach( var file in rightFileList )
      {
        if( !leftFileList.Any( x => x.Item1 == file.Item1 ) )
        {
          addedFiles.Add( file );
        }
      }

      foreach( var file in leftFileList )
      {
        int index = rightFileList.FindIndex( x => x.Item1 == file.Item1 );
        if( index != -1 )
        {
          JToken patch = _jsonDiffPatch.Diff( file.Item2, rightFileList[index].Item2 );
          if( patch != null )
          {
            changedFiles.Add( new Tuple<string, JToken>( file.Item1, patch ) );
          }
        }
        else
        {
          removedFiles.Add( file );
        }
      }

      return new ConfigCheckResult( addedFiles, removedFiles, changedFiles );
    }
  }
}
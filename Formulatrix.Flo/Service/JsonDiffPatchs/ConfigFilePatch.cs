﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace Formulatrix.Flo.Service.JsonDiffPatchs
{
  public class ConfigFilePatch
  {
    public List<ConfigFilePatchDetail> ConfigFilePatches { get; }

    public ConfigFilePatch( List<ConfigFilePatchDetail> configFilePatches )
    {
      ConfigFilePatches = configFilePatches;
    }
  }

  public class ConfigFilePatchDetail
  {
    [JsonConverter( typeof( StringEnumConverter ) )]
    public PatchAction Action { get; }
    public string Filename { get; }
    public JToken Content { get; }

    public ConfigFilePatchDetail( PatchAction action, string filename, JToken content )
    {
      Action = action;
      Filename = filename;
      Content = content;
    }
  }
}

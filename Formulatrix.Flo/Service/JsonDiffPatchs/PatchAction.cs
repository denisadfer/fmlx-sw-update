﻿namespace Formulatrix.Flo.Service.JsonDiffPatchs
{
  public enum PatchAction
  {
    Change,
    Add,
    Remove
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Newtonsoft.Json.Linq;

namespace Formulatrix.Flo.Service.JsonDiffPatchs
{
  public class ConfigFileLoader
  {
    const string JSON_FILE_EXTENSIONS = "json";

    public ErrorCode LoadFiles( string directory, out List<Tuple<string, JToken>> fileList )
    {
      fileList = new List<Tuple<string, JToken>>();
      ErrorCode errorCode =  Load( directory, directory, fileList );
      return errorCode;
    }

    private ErrorCode Load( string parrentDirectory, string directory, List<Tuple<string, JToken>> fileList )
    {
      foreach( string file in Directory.EnumerateFiles( directory ) )
      {
        try
        {
          if( file.Split( '.' ).Last().Equals( JSON_FILE_EXTENSIONS ) )
          {
            JToken jToken = JToken.Parse( File.ReadAllText( file ) );
            fileList.Add( new Tuple<string, JToken>( file.Replace( parrentDirectory, string.Empty ), jToken ) );
          }
        }
        catch
        {
          return ErrorCode.ParsingDataFailed;
        }
      }

      string[] directories = Directory.GetDirectories( directory );
      if( directories != null )
      {
        foreach( string directoryItem in directories )
        {
          Load( parrentDirectory, directoryItem, fileList );
        }
      }

      return ErrorCode.NoError;
    }
  }
}

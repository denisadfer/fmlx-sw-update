using System.IO;
using System.Text;
using Renci.SshNet;

namespace Formulatrix.Flo.Service.Shell
{
  public class SshService : IService
  {
    private readonly SshClient _sshClient;

    public SshService( string ipAddress, string username, string password )
    {
      var connectionInfo = new ConnectionInfo( ipAddress,
                                               username,
                                               new PasswordAuthenticationMethod( username, password ) );

      _sshClient = new SshClient( connectionInfo );
    }
    
    public ErrorCode Connect()
    {
      try
      {
        _sshClient.Connect();
        return ErrorCode.NoError;
      }
      catch
      {
        return ErrorCode.ConnectionFailed;
      }
    }

    public ErrorCode SendCommand( string command, out string errorCommand )
    {
      try
      {
        var cmd = _sshClient.CreateCommand( command );
        var results = cmd.BeginExecute();
        using( var reader = new StreamReader( cmd.ExtendedOutputStream, Encoding.UTF8, true, 1024, true ) )
        {
          while( !results.IsCompleted || !reader.EndOfStream )
          {
            var result = reader.ReadLine();
            if( !cmd.Error.Equals( string.Empty ) )
            {
              errorCommand = cmd.Error;
              return ErrorCode.ConnectionFailed;
            }
            if( result != null )
            {
              //do something;
            }
            if( reader.EndOfStream )
            {
              break;
            }
          }
        }
        errorCommand = string.Empty;
        return ErrorCode.NoError;
      }
      catch
      {
        errorCommand = string.Empty;
        return ErrorCode.ConnectionFailed;
      }
    }

    public void Disconnect()
    {
      if( _sshClient != null && _sshClient.IsConnected )
      {
        _sshClient.Disconnect();
      }
    }
  }
}

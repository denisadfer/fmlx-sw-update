using System;
using System.Collections.Generic;
using System.IO;
using Renci.SshNet;
using Renci.SshNet.Sftp;

namespace Formulatrix.Flo.Service.Shell
{
  public class SftpService : IService
  {
    private readonly SftpClient _sftpClient;

    public SftpService( string ipAddress, string username, string password )
    {
      var connectionInfo = new ConnectionInfo( ipAddress,
        username,
        new PasswordAuthenticationMethod( username, password ) );

      _sftpClient = new SftpClient( connectionInfo );
    }

    public ErrorCode Connect()
    {
      try
      {
        _sftpClient.Connect();
        return ErrorCode.NoError;
      }
      catch
      {
        return ErrorCode.ConnectionFailed;
      }
    }

    public bool IsConnected()
    {
      return _sftpClient.IsConnected;
    }

    public void Disconnect()
    {
      if( _sftpClient != null && _sftpClient.IsConnected )
      {
        _sftpClient.Disconnect();
      }
    }

    public ErrorCode UploadFile( FileStream fileStream, string destinationPath, Action<ulong> callback = null )
    {
      try
      {
        _sftpClient.BufferSize = 4 * 1024;
        _sftpClient.UploadFile( fileStream, destinationPath, callback );
        return ErrorCode.NoError;
      }
      catch
      {
        return ErrorCode.ConnectionFailed;
      }
    }

    public ErrorCode DownloadFile( string sourcePath, FileStream fileStream, Action<ulong> callback = null )
    {
      try
      {
        _sftpClient.DownloadFile( sourcePath, fileStream, callback );
        return ErrorCode.NoError;
      }
      catch
      {
        return ErrorCode.ConnectionFailed;
      }
    }

    public ErrorCode ListDirectory( string path, out IEnumerable<SftpFile> listDirectory )
    {
      try
      {
        listDirectory = _sftpClient.ListDirectory( path );
        return ErrorCode.NoError;
      }
      catch
      {
        listDirectory = null;
        return ErrorCode.ConnectionFailed;
      }
    }

    public bool IsDirectoryExist( string path )
    {
      return _sftpClient.Exists( path );
    }

    public ErrorCode CreateDirectory( string path )
    {
      try
      {
        _sftpClient.CreateDirectory( path );
        return ErrorCode.NoError;
      }
      catch
      {
        return ErrorCode.ConnectionFailed;
      }
    }

    public ErrorCode SetPermission( string path, short permission )
    {
      try
      {
        _sftpClient.ChangePermissions( path, permission );
        return ErrorCode.NoError;
      }
      catch
      {
        return ErrorCode.ConnectionFailed;
      }
    }
  }
}

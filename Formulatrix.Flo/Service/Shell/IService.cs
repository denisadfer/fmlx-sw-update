namespace Formulatrix.Flo.Service.Shell
{
  public interface IService
  {
    ErrorCode Connect();
    void Disconnect();
  }
}

﻿namespace Formulatrix.Flo
{
  public enum ErrorCode
  {
    NoError,

    ConnectionTimeout,
    ConnectionFailed,

    ParsingDataFailed,
    ModifyingDataFailed,

    ServiceNotExists,
    DirectoryNotFound,
    FileNotFound,
    SetPermissionFailed,
    InvalidFile,
    
    UpdateFirmwareFailed,
    SystemUpdateFailed
  }
}

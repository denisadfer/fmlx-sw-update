﻿using Newtonsoft.Json;

namespace Formulatrix.Flo.SystemUpdater.ConfigDefs
{
  public class FloSystemUpdateConfig
  {
    [JsonProperty( Required = Required.Always )]
    public string SystemUpdateVersionFilePath { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    public string FileUpdatePath { get; set; }

    [JsonProperty( Required = Required.Always )]
    public string DeploymentTemporaryDirectoryPath { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    public bool SimMode { get; set; }
  }
}

﻿using Formulatrix.Flo.SystemUpdater.ConfigDefs;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Formulatrix.Flo.SystemUpdater
{
  public class SystemUpdateService
  {
    protected readonly FloSystemUpdateConfig ConfigSystemUpdate;
    protected readonly ILogger<SystemUpdateService> Logger;

    public string LastCommand { get; private set; }

    public FsuErrorCode UpdaterErrorCode { get; private set; }

    private string _commandResult;
    public string CommandResult
    {
      get => _commandResult;
      protected set => _commandResult = value;
    }

    private int _commandExitCode;
    public int CommandExitCode
    {
      get => _commandExitCode;
      protected set => _commandExitCode = value;
    }

    public SystemUpdateService( FloSystemUpdateConfig configSystemUpdate, ILogger<SystemUpdateService> logger )
    {
      ConfigSystemUpdate = configSystemUpdate;
      Logger = logger;
    }

    protected FsuErrorCode ExtractNecessaryFiles(string packageFilePath, out string packageVersion)
    {
      string logCtx = $"ExtractNecessaryFiles(packageFilePath:\"{packageFilePath}\", out string packageVersion:null))";
      Logger?.LogDebug( $"START [{logCtx}]" );
      
      packageVersion = null;

      string workDir = ConfigSystemUpdate.DeploymentTemporaryDirectoryPath;
      
      string tarCommandPath = "/usr/bin/tar";
      string tarCommandArgs = $"jxf {packageFilePath} version fsup";
      LastCommand = $"{tarCommandPath} {tarCommandArgs}";
      
      if( LinuxCommandExitCode.Ok != LinuxCommand.Run( tarCommandPath, tarCommandArgs, workDir,
           out _commandResult, out _commandExitCode ) )
      {
        UpdaterErrorCode = FsuErrorCode.RunCommandExecutionFailed;
        
        Logger?.LogError( $"ERROR [{logCtx}]: {UpdaterErrorCode}; " +
                         $"command '{LastCommand}' execution failed, " +
                         $"errcode:{_commandExitCode}, errmessage:'{_commandResult}'" );
        
        // command execution failed - refer to 'LastCommand' to determine which command failed
        return UpdaterErrorCode;
      }

      // command exit code non zero? Necessary files found?
      if( _commandExitCode != 0 )
      {
        // command execution succeeded but result indicates failure OR version is empty (update package corrupted?)
        // refer to 'LastCommand' to determine which command failed
        UpdaterErrorCode = FsuErrorCode.RunCommandReturnNonZero;
        
        Logger?.LogError( $"ERROR [{logCtx}]: {UpdaterErrorCode}; " +
                         $"command '{LastCommand}' extract failed, return non-zero errcode, " +
                         $"errcode:{_commandExitCode}, errmessage:'{_commandResult}'" );
        
        return UpdaterErrorCode;
      }

      if( !File.Exists( $"{workDir}/version" ) || !File.Exists( $"{workDir}/fsup" ) )
      {
        UpdaterErrorCode = FsuErrorCode.RunCommandReturnNonZero;
        
        Logger?.LogError( $"ERROR [{logCtx}]: {UpdaterErrorCode}; " +
                          $"command '{LastCommand}' necessary files not extracted" +
                          $"errcode:{_commandExitCode}, errmessage:'{_commandResult}'" );
        
        return UpdaterErrorCode;
      }
      
      // version
      packageVersion = File.ReadAllText( $"{workDir}/version" ).Trim();
      File.Delete( $"{workDir}/version" );

      string updateWorkDir = $"{workDir}/{packageVersion}";

      if( Directory.Exists( updateWorkDir ) )
        Directory.Move( updateWorkDir, updateWorkDir + "_" + Guid.NewGuid() );
      Directory.CreateDirectory( updateWorkDir );
      File.Move( $"{workDir}/fsup", $"{updateWorkDir}/fsup" );
      
      UpdaterErrorCode = FsuErrorCode.NoError;

      Logger?.LogDebug(
        $"FINISH [{logCtx}]; results:[packageVersion:\"{packageVersion}\", return {UpdaterErrorCode}]" );
      return UpdaterErrorCode;
    }

    private FsuErrorCode DeployPackage( string compressedSystemUpdatePackageFilePath, string packageVersion,
      string prevVersionInfoFilePath, string extractDestination )
    {
      string logCtx = "DeployPackage(" +
                      $"compressedSystemUpdatePackageFilePath:\"{compressedSystemUpdatePackageFilePath}\", " +
                      $"packageVersion:\"{packageVersion}\", extractDestination:\"{extractDestination}\"))";
      Logger?.LogDebug( $"START [{logCtx}]" );
      
      string updateWorkDir = $"{extractDestination}/{packageVersion}";

      // - sudo FSUP_TEMP_DIR="$extractDestination" ./f version deploy $compressedSystemUpdatePackageFilePath
      string deployCommand = $"{updateWorkDir}/fsup";
      string deployCommandArgs = $"{packageVersion} deploy \"{compressedSystemUpdatePackageFilePath}\"";
      IDictionary<string, string> envCommand = new Dictionary<string, string>
      {
        {"FSUP_VER_PATH", prevVersionInfoFilePath}, {"FSUP_TEMP_DIR", updateWorkDir},
      };

      LastCommand = $"FSUP_VER_PATH={prevVersionInfoFilePath} FSUP_TEMP_DIR={updateWorkDir} " +
                    $"{deployCommand} {deployCommandArgs}";

      if( LinuxCommandExitCode.Ok != LinuxCommand.Run(
           deployCommand, deployCommandArgs, updateWorkDir, envCommand,
           out _commandResult, out _commandExitCode ) )
      {
        UpdaterErrorCode = FsuErrorCode.RunCommandExecutionFailed;

        Logger?.LogError( $"ERROR [{logCtx}]: {UpdaterErrorCode}; " +
                          "command '{LastCommand}' return non-zero errcode, " +
                          "errcode:{_commandExitCode}, errmessage:'{_commandResult}'" );

        // refer to 'LastCommand' to determine which command failed
        return UpdaterErrorCode;
      }

      if( _commandExitCode != 0 )
      {
        UpdaterErrorCode = FsuErrorCode.RunCommandReturnNonZero;

        Logger?.LogError( $"ERROR [{logCtx}]: {UpdaterErrorCode}; " +
                          $"command '{LastCommand}' return non-zero errcode, " +
                          $"errcode:{_commandExitCode}, errmessage:'{_commandResult}'" );

        // refer to 'LastCommand' to determine which command failed
        return UpdaterErrorCode;
      }

      UpdaterErrorCode = FsuErrorCode.NoError;

      Logger?.LogDebug( $"FINISH [{logCtx}]; results:[return {UpdaterErrorCode}]" );

      return UpdaterErrorCode;
    }

    public ErrorCode DoUpdate()
    {
      string logCtx = "DoUpdate()";
      
      Logger?.LogDebug( $"START [{logCtx}]" );

      string prevVersionInfoFilePath = ConfigSystemUpdate.SystemUpdateVersionFilePath;
      string compressedSystemUpdatePackageFilePath = ConfigSystemUpdate.FileUpdatePath;
      string extractDestination = ConfigSystemUpdate.DeploymentTemporaryDirectoryPath;
      
      Logger?.LogDebug( $"[{logCtx}] extractDestination: '{extractDestination}'" );
      
      // create temporary directory
      if( !Directory.Exists( extractDestination ) )
        Directory.CreateDirectory( extractDestination );

      // extract version
      if( FsuErrorCode.NoError !=
          ExtractNecessaryFiles( compressedSystemUpdatePackageFilePath, out string packageVersion ) )
      {
        return ErrorCode.SystemUpdateFailed;
      }

      // execute deploy
      if( FsuErrorCode.NoError != DeployPackage( compressedSystemUpdatePackageFilePath, packageVersion,
           prevVersionInfoFilePath, extractDestination ) )
      {
        return ErrorCode.SystemUpdateFailed;
      }

      Logger?.LogDebug( $"FINISH [{logCtx}]; results:[return {ErrorCode.NoError}]" );

      return ErrorCode.NoError;
    }
    
    public ErrorCode DoRevert(string packageVersion)
    {
      string rollbackSource = $"{ConfigSystemUpdate.DeploymentTemporaryDirectoryPath}/{packageVersion}";
      
      string logCtx = $"DoRevert(packageVersion:\"{packageVersion}\")";
      
      Logger?.LogDebug( $"START [{logCtx}]" );
      
      if( FsuErrorCode.NoError !=
          Rollback( packageVersion, rollbackSource ) )
      {
        return ErrorCode.SystemUpdateFailed;
      }

      Logger?.LogDebug( $"FINISH [{logCtx}]; results:[return {ErrorCode.NoError}]" );
      
      return ErrorCode.NoError;
    }

    private FsuErrorCode Rollback( string packageVersion, string deploymentDirectory )
    {
      string logCtx = $"Rollback(packageVersion:{packageVersion}, deploymentDirectory:'{deploymentDirectory}')";
      Logger?.LogDebug( $"START [{logCtx}]" );

      // execute rollback
      string rollbackCommand = $"{deploymentDirectory}/fsup";
      string rollbackCommandArgs = $"{packageVersion} rollback \"{deploymentDirectory}/{packageVersion}\"";

      LastCommand = $"{rollbackCommand} {rollbackCommandArgs}";

      Console.WriteLine( LastCommand );

      if( LinuxCommandExitCode.Ok != LinuxCommand.Run(
           rollbackCommand, rollbackCommandArgs, deploymentDirectory,
           out _commandResult, out _commandExitCode ) )
      {
        UpdaterErrorCode = FsuErrorCode.RunCommandExecutionFailed;

        Logger?.LogError( $"ERROR [{logCtx}]: {UpdaterErrorCode}; " +
                          $"command '{LastCommand}' return non-zero errcode, " +
                          $"errcode:{_commandExitCode}, errmessage:'{_commandResult}'" );

        // refer to 'LastCommand' to determine which command failed
        return UpdaterErrorCode;
      }
      
      if( _commandExitCode != 0 )
      {
        UpdaterErrorCode = FsuErrorCode.RunCommandReturnNonZero;

        Logger?.LogError( $"ERROR [{logCtx}]: {UpdaterErrorCode}; " +
                          $"command '{LastCommand}' return non-zero errcode, " +
                          $"errcode:{_commandExitCode}, errmessage:'{_commandResult}'" );

        // refer to 'LastCommand' to determine which command failed
        return UpdaterErrorCode;
      }

      UpdaterErrorCode = FsuErrorCode.NoError;

      Logger?.LogDebug( $"FINISH [{logCtx}]; results:[return {UpdaterErrorCode}]" );

      return UpdaterErrorCode;
    }
  }
}

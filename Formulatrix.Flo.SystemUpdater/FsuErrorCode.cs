﻿namespace Formulatrix.Flo.SystemUpdater
{
  public enum FsuErrorCode
  {
    NoError = 0,
    RunCommandExecutionFailed = 100,
    RunCommandReturnNonZero = 101,
  }
}
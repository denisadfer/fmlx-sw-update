﻿using System.Collections.Generic;

namespace Formulatrix.Flo.SystemUpdater
{
  public static class LinuxCommand
  {
    private static string _commandResult;
    private static int _exitCode;

    public static string CommandResult => _commandResult;
    public static int ExitCode => _exitCode;

    public static LinuxCommandExitCode Run(
      string executablePath, string arguments, string workdir, IDictionary<string, string> environmentVars,
      out string commandResult, out int commandExitCode)
    {
      System.Diagnostics.ProcessStartInfo process = new System.Diagnostics.ProcessStartInfo
      {
        UseShellExecute = false,
        WorkingDirectory = workdir,
        FileName = executablePath,
        Arguments = arguments,
        RedirectStandardOutput = true,
      };

      if( environmentVars != null )
      {
        process.Environment.Clear();
        foreach( string key in environmentVars.Keys )
        {
          process.Environment[key] = environmentVars[key];
        }
      }

      _commandResult = commandResult = null;
      _exitCode = commandExitCode = -1;

      using( System.Diagnostics.Process command = System.Diagnostics.Process.Start( process ) )
      {
        if( command == null )
          return LinuxCommandExitCode.GeneralFailure;

        command.WaitForExit();
        
        _commandResult = commandResult = command.StandardOutput.ReadToEnd();
        _exitCode = commandExitCode = command.ExitCode;
      }

      return LinuxCommandExitCode.Ok;
    }

    public static LinuxCommandExitCode Run(
      string executablePath, string arguments, string workdir,
      out string commandResult, out int commandExitCode )
      => Run( executablePath, arguments, workdir, null, out commandResult, out commandExitCode );

    public static LinuxCommandExitCode Run(
      string executablePath, string arguments, string workdir,
      out string commandResult )
      => Run( executablePath, arguments, workdir, null, out commandResult, out _exitCode );

    public static LinuxCommandExitCode Run(
      string executablePath, string arguments, string workdir )
      => Run( executablePath, arguments, workdir, null, out _commandResult, out _exitCode );
  }
}
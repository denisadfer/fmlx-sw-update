namespace Formulatrix.Flo.SystemUpdater
{
  public enum LinuxCommandExitCode
  {
    Ok = 0,
    GeneralFailure = 1,
  }
}
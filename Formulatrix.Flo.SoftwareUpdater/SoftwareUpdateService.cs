﻿using System;
using System.IO;
using System.Linq;
using Formulatrix.Flo.SoftwareUpdater.ConfigDefs;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace Formulatrix.Flo.SoftwareUpdater
{
  public class SoftwareUpdateService
  {
    private readonly ILogger _logger;

    private readonly FloSoftwareUpdateConfig _config;

    private const string UPDATE_FLAG_NAME = "updateFlag.txt";
    private const string DATE_TIME_FORMAT = "MM/dd/yyyy HH:mm";

    public SoftwareUpdateService( FloSoftwareUpdateConfig config, ILogger logger )
    {
      _logger = logger;

      _config = config;
    }

    public ErrorCode DoUpdate()
    {
      _logger.LogDebug( "DoUpdate,start" );

      var errorCode = FileManager.CopyDirectory( _config.FileUpdatePath,
                                             _config.BinaryPath,
                                             true,
                                             FileManager.NOT_EQUAL,
                                             new[] { "ConfigFile", "Database" },
                                             true,
                                             _logger );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,end,errorCode={errorCode}" );
        return errorCode;
      }

      errorCode = FileManager.SetPermission( _config.ApplicationPath, FileManager.PERMISSION775 );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,end,errorCode={errorCode}" );
        return errorCode;
      }

      var assemblyPath = Path.Combine( _config.BinaryPath, _config.AssemblyName );
      var flagPath = _config.BinaryPath;
      WriteUpdateFlag( assemblyPath, flagPath );

      _logger.LogDebug( $"DoUpdate,end,errorCode={errorCode}" );
      return errorCode;
    }

    private void WriteUpdateFlag( string assemblyPath, string flagPath )
    {
      try
      {
        var version = FileVersionInfo.GetVersionInfo( assemblyPath ).ProductVersion;
        File.WriteAllText( Path.Combine( flagPath, UPDATE_FLAG_NAME ), string.Concat( version, " ", DateTime.Now.ToString( DATE_TIME_FORMAT ) ) );
      }
      catch
      {
        _logger.LogDebug( "WriteUpdateFlag,Cannot load assembly file" );
      }
    }
  }
}

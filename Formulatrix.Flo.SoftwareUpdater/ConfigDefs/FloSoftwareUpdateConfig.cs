﻿using Newtonsoft.Json;

namespace Formulatrix.Flo.SoftwareUpdater.ConfigDefs
{
  public class FloSoftwareUpdateConfig
  {
    [JsonProperty( Required = Required.Always )]
    public string BinaryPath { get; set; }

    [JsonProperty( Required = Required.Always )]
    public string FileUpdatePath { get; set; }

    [JsonProperty( Required = Required.Always )]
    public string BackupPath { get; set; }

    [JsonProperty( Required = Required.Always )]
    public string ApplicationPath { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    public string AssemblyName { get; set; }

    [JsonProperty( Required = Required.Always )]
    public string FloServiceName { get; set; }

    [JsonProperty( Required = Required.Always )]
    public string UpdaterServiceName { get; set; }

    [JsonProperty( Required = Required.Always )]
    public bool SimMode { get; set; }
  }
}

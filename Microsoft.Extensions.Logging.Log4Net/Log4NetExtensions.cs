﻿using System;

namespace Microsoft.Extensions.Logging.Log4Net
{
  public static class Log4netExtensions
  {
    /// <summary>
    /// The default log4net config file name.
    /// </summary>
    private const string DEFAULT_LOG4NET_CONFIG_FILE = "log4net.config";

    /// <summary>
    /// Adds the log4net.
    /// </summary>
    /// <param name="factory">The factory.</param>
    /// <param name="log4NetConfigFile">The log4net Config File.</param>
    /// <returns>The <see cref="ILoggerFactory"/>.</returns>
    public static ILoggerFactory AddLog4Net( this ILoggerFactory factory, string log4NetConfigFile, Func<object, Exception, string> exceptionFormatter )
    {
      factory.AddProvider( new Log4NetProvider( log4NetConfigFile, exceptionFormatter ) );
      return factory;
    }

    /// <summary>
    /// Adds the log4net.
    /// </summary>
    /// <param name="factory">The factory.</param>
    /// <param name="log4NetConfigFile">The log4net Config File.</param>
    /// <returns>The <see cref="ILoggerFactory"/>.</returns>
    public static ILoggerFactory AddLog4Net( this ILoggerFactory factory, string log4NetConfigFile )
    {
      factory.AddProvider( new Log4NetProvider( log4NetConfigFile ) );
      return factory;
    }

    /// <summary>
    /// Adds the log4net.
    /// </summary>
    /// <param name="factory">The factory.</param>
    /// <param name="exceptionFormatter">The exception formatter.</param>
    /// <returns>The <see cref="ILoggerFactory"/>.</returns>
    public static ILoggerFactory AddLog4Net( this ILoggerFactory factory, Func<object, Exception, string> exceptionFormatter )
    {
      factory.AddLog4Net( DEFAULT_LOG4NET_CONFIG_FILE, exceptionFormatter );
      return factory;
    }

    /// <summary>
    /// Adds the log4net.
    /// </summary>
    /// <param name="factory">The factory.</param>
    /// <returns>The <see cref="ILoggerFactory"/>.</returns>
    public static ILoggerFactory AddLog4Net( this ILoggerFactory factory )
    {
      factory.AddLog4Net( DEFAULT_LOG4NET_CONFIG_FILE );
      return factory;
    }
  }
}

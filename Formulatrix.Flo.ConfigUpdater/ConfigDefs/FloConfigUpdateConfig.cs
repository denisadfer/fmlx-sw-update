﻿using Newtonsoft.Json;

namespace Formulatrix.Flo.ConfigUpdater.ConfigDefs
{
  public class FloConfigUpdateConfig
  {
    [JsonProperty( Required = Required.Always )]
    public string ConfigFilePath { get; set; }

    [JsonProperty( Required = Required.Always )]
    public string FileUpdatePath { get; set; }

    [JsonProperty( Required = Required.Always )]
    public string DefaultConfigFilePath { get; set; }

    [JsonProperty( Required = Required.Always )]
    public string BackupPath { get; set; }

    [JsonProperty( Required = Required.Always )]
    public bool SimMode { get; set; }
    
    public string[] OverwriteFiles{ get; set; }
  }
}

﻿using Formulatrix.Flo.ConfigUpdater.ConfigDefs;
using Formulatrix.Flo.Service.JsonDiffPatchs;
using Microsoft.Extensions.Logging;
using System.IO;

namespace Formulatrix.Flo.ConfigUpdater
{
  public class ConfigUpdateService
  {
    private readonly ILogger _logger;

    private readonly FloConfigUpdateConfig _config;

    private readonly JsonDiffPatchService _jsonDiffPatchService;

    public ConfigUpdateService( FloConfigUpdateConfig config, JsonDiffPatchService jsonDiffPatchService, ILogger logger )
    {
      _logger = logger;

      _config = config;
      _jsonDiffPatchService = jsonDiffPatchService;
    }

    public ErrorCode DoUpdate()
    {
      _logger.LogDebug( "DoUpdate,start" );

      var errorCode = _jsonDiffPatchService.GeneratePatch( _config.DefaultConfigFilePath, _config.FileUpdatePath, out string jsonPatch );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,end,errorCode={errorCode}" );
        return errorCode;
      }

      errorCode = _jsonDiffPatchService.ApplyPatch( jsonPatch, _config.ConfigFilePath );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,end,errorCode={errorCode}" );
        return errorCode;
      }

      errorCode = FileManager.CleanDirectory( _config.DefaultConfigFilePath );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,end,errorCode={errorCode}" );
        return errorCode;
      }
      
      errorCode = FileManager.CopyDirectory( _config.FileUpdatePath,
                                             _config.DefaultConfigFilePath,
                                             true );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"DoUpdate,end,errorCode={errorCode}" );
        return errorCode;
      }

      foreach( var file in _config.OverwriteFiles )
      {
        errorCode = FileManager.CopyFile( _config.FileUpdatePath, 
          _config.ConfigFilePath, file, _logger );

        if( errorCode != ErrorCode.NoError )
        {
          _logger.LogError( $"DoUpdate,end,errorCode={errorCode}" );
          return errorCode;
        }
      }

      _logger.LogDebug( $"DoUpdate,end,errorCode={errorCode}" );
      return errorCode;
    }
  }
}

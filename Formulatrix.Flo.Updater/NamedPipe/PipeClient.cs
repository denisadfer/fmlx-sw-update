﻿using System;
using System.IO.Pipes;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Formulatrix.Flo.Updater.NamedPipe
{
  public class PipeClient : PipeBase
  {
    private const string SERVER_NAME = ".";
    private const string PIPE_NAME = "FloPipeServer";
    private const int TIMEOUT = 5000;

    private readonly NamedPipeClientStream _pipeClientStream;

    private readonly ILogger _logger;

    public PipeClient( Action<PipeBase> asyncReaderStart, ILogger logger ) : base( asyncReaderStart )
    {
      _logger = logger;

      _pipeClientStream = new NamedPipeClientStream( SERVER_NAME, PIPE_NAME, PipeDirection.InOut, PipeOptions.Asynchronous );
      PipeStream = _pipeClientStream;
    }

    public ErrorCode Connect()
    {
      _logger.LogDebug( $"Connect,serverName={PIPE_NAME},start" );
      ErrorCode errorCode = ErrorCode.NoError;
      try
      {
        _pipeClientStream.Connect( TIMEOUT );
        AsyncReaderStart( this );
        _logger.LogDebug( $"Connect,serverName={PIPE_NAME},end,errorCode={errorCode}" );
      }
      catch( TimeoutException )
      {
        errorCode = ErrorCode.ConnectionTimeout;
        _logger.LogError( $"Connect,serverName={PIPE_NAME},end,errorCode={errorCode}" );
      }
      return errorCode;
    }

    public async void SendCommand( PipeMessage message )
    {
      string stringMessage = JsonConvert.SerializeObject( message, new StringEnumConverter() );
      await WriteString( stringMessage );
      _pipeClientStream.Flush();
    }
  }
}

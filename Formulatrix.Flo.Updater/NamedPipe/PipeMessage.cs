﻿using Newtonsoft.Json;

namespace Formulatrix.Flo.Updater.NamedPipe
{
  public class PipeMessage
  {
    public PipeMessageType MessageType { get; }
    public object Payload { get; }

    [JsonConstructor]
    public PipeMessage( PipeMessageType messageType, object payload = null )
    {
      MessageType = messageType;
      Payload = payload;
    }
  }
}

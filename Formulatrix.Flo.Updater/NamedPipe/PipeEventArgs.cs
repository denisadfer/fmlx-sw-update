﻿namespace Formulatrix.Flo.Updater.NamedPipe
{
  public class PipeEventArgs
  {
    public string String { get; }

    public PipeEventArgs( string str )
    {
      String = str;
    }
  }
}

﻿using System;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formulatrix.Flo.Updater.NamedPipe
{
  public abstract class PipeBase
  {
    public event EventHandler<PipeEventArgs> DataReceived;
    public event EventHandler<EventArgs> PipeClosed;

    protected PipeBase( Action<PipeBase> asyncReaderStart )
    {
      AsyncReaderStart = asyncReaderStart;
    }

    protected PipeStream PipeStream { get; set; }
    protected Action<PipeBase> AsyncReaderStart { get; }

    public void StartStringReaderAsync()
    {
      StartByteReaderAsync( ( packet ) =>
      {
        string str = Encoding.UTF8.GetString( packet ).TrimEnd( '\0' );
        DataReceived?.Invoke( this, new PipeEventArgs( str ) );
      } );
    }

    private void StartByteReaderAsync( Action<byte[]> packetReceived )
    {
      int offset = sizeof( int );
      byte[] buffer = new byte[offset];

      PipeStream.ReadAsync( buffer, 0, offset ).ContinueWith( task =>
      {
        int result = task.Result;
        if( result == 0 )
        {
          PipeClosed?.Invoke( this, EventArgs.Empty );
        }
        else
        {
          int dataLength = BitConverter.ToInt32( buffer, 0 );
          byte[] data = new byte[dataLength];

          PipeStream.ReadAsync( data, 0, dataLength ).ContinueWith( nextTask =>
          {
            packetReceived( data );
          } );
          StartByteReaderAsync( packetReceived );
        }
      } );
    }

    protected Task WriteString( string str )
    {
      return WriteBytes( Encoding.UTF8.GetBytes( str ) );
    }

    private Task WriteBytes( byte[] bytes )
    {
      var bytesLength = BitConverter.GetBytes( bytes.Length );
      var bytesFull = bytesLength.Concat( bytes ).ToArray();

      return PipeStream.WriteAsync( bytesFull, 0, bytesFull.Length );
    }
  }
}

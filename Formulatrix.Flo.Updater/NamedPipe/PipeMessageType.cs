﻿namespace Formulatrix.Flo.Updater.NamedPipe
{
  public enum PipeMessageType
  {
    UpdateCommand,
    RestartCommand,

    ClientReady,
    UpdateStart,
    UpdateProgress,
    UpdateTerminated,
    UpdateDone,
    RestartInit,
    RestartStart,
    RestartFailed,
    
    FirmwareVersionInfo,
    UpdateFirmwareInit,
    
    VisionVersionInfo
  }
}

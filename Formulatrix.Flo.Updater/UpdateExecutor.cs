using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Formulatrix.Flo.ConfigUpdater;
using Formulatrix.Flo.FirmwareUpdater;
using Formulatrix.Flo.SoftwareUpdater;
using Formulatrix.Flo.SystemUpdater;
using Formulatrix.Flo.Updater.ConfigDefs;
using Formulatrix.Flo.Updater.NamedPipe;
using Formulatrix.Flo.VisionUpdater;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace Formulatrix.Flo.Updater
{
  public class UpdateExecutor
  {
    private readonly FloUpdateConfig _config;
    private readonly PipeClient _pipeClient;

    private readonly SystemUpdateService _systemUpdate;
    private readonly ConfigUpdateService _configUpdate;
    private readonly SoftwareUpdateService _softwareUpdate;
    private readonly FirmwareUpdateService _firmwareUpdate;
    private readonly VisionUpdateService _visionUpdate;

    private Dictionary<string, string> _firmwareVersionInfo;
    private string _visionVersionInfo;
    private readonly AutoResetEvent _responseEvent;

    private readonly ILogger _logger;

    private const string UPDATE_STAGE_NAME = "updateStage.txt";
    public UpdateExecutor( FloUpdateConfig config, PipeClient pipeClient, SystemUpdateService systemUpdate,
                           ConfigUpdateService configUpdate, SoftwareUpdateService softwareUpdate,
                           FirmwareUpdateService firmwareUpdate, VisionUpdateService visionUpdate,
                           ILogger logger )
    {
      _config = config;
      _pipeClient = pipeClient;

      _systemUpdate = systemUpdate;

      _configUpdate = configUpdate;
      _softwareUpdate = softwareUpdate;
      _firmwareUpdate = firmwareUpdate;
      _visionUpdate = visionUpdate;

      _logger = logger;

      _pipeClient.DataReceived += OnPipeClientDataReceived;
      _pipeClient.PipeClosed += OnPipeClosed;

      _responseEvent = new AutoResetEvent( false );
    }

    public void Init()
    {
      ErrorCode errorCode = _pipeClient.Connect();
      if( errorCode == ErrorCode.NoError )
      {
        SendUpdateProgress( new PipeMessage( PipeMessageType.ClientReady ) );
      }
    }

    private void OnPipeClientDataReceived( object sender, PipeEventArgs e )
    {
      try
      {
        var pipeMessage = JsonConvert.DeserializeObject<PipeMessage>( e.String );

        _logger.LogDebug( $"OnPipeServerDataReceived,message={JsonConvert.SerializeObject( pipeMessage, new StringEnumConverter() )}" );

        switch( pipeMessage?.MessageType )
        {
          case PipeMessageType.UpdateCommand:
            {
              if( pipeMessage.Payload is JArray payload )
              {
                var userId = Convert.ToInt16( payload[0] );
                var stage = Convert.ToInt16( payload[1] );
                DoUpdate( userId, stage );
              }
              else
              {
                var userId = Convert.ToInt16( pipeMessage.Payload );
                DoUpdate( userId );
              }
            }
            break;
          case PipeMessageType.RestartCommand:
            {
              var userId = Convert.ToInt16( pipeMessage.Payload );
              DoRestart( userId );
            }
            break;
          case PipeMessageType.FirmwareVersionInfo:
            {
              var json = JsonConvert.SerializeObject( pipeMessage.Payload );
              _firmwareVersionInfo = JsonConvert.DeserializeObject<Dictionary<string, string>>( json );
              _responseEvent.Set();
            }
            break;
          case PipeMessageType.UpdateFirmwareInit:
            _responseEvent.Set();
            break;
          case PipeMessageType.VisionVersionInfo:
            {
              _visionVersionInfo = pipeMessage.Payload.ToString();
              _responseEvent.Set();
            }
            break;
          default:
            return;
        }
      }
      catch( Exception ex )
      {
        _logger.LogCritical( ex.StackTrace );
        throw;
      }
    }

    private ErrorCode DoUpdate( int userId, int stage = 0 )
    {
      _logger.LogDebug( "DoUpdate,start" );
      ErrorCode errorCode = ErrorCode.NoError;

      SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateStart ) );
      SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateProgress, 0 ) );
      
      switch( stage )
      {
        case 2:
          goto Stage2;
        case 3:
          goto Stage3;
        case 4:
          goto Stage4;
        case 5:
          goto Stage5;
      }
      
      #region Stage 0: System Update (includes backup data for rollback)
      SaveUpdateStage( userId, 0, UpdateState.Start );
      if( _config.SystemUpdate.SimMode )
      {
        SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateProgress, 10 ) );
        Thread.Sleep( 5000 );
      }
      else
      {
        SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateProgress, 10 ) );
        errorCode = _systemUpdate.DoUpdate();
        if( errorCode != ErrorCode.NoError )
        {
          SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateTerminated ) );
          _logger.LogError( $"_systemUpdate.DoUpdate,end,errorCode={errorCode}" );
          return errorCode;
        }
      }
      SaveUpdateStage( userId, 0, UpdateState.Done );
      #endregion
      
      #region Stage 1: Backup FloWebServer
      SaveUpdateStage( userId, 1, UpdateState.Start );
      if( _config.SoftwareUpdate.SimMode )
      {
        SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateProgress, 20 ) );
        Thread.Sleep( 5000 );
      }
      else
      {
        SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateProgress, 20 ) );
        errorCode = Backup();
        if( errorCode != ErrorCode.NoError )
        {
          SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateTerminated, errorCode ) );
          _logger.LogError( $"DoUpdate,end,errorCode={errorCode}" );
          return errorCode;
        }
      }
      SaveUpdateStage( userId, 1, UpdateState.Done );
      #endregion

      #region Stage 2: Update Config FloWebServer
      Stage2:
      SaveUpdateStage( userId, 2, UpdateState.Start );
      if( _config.ConfigUpdate.SimMode )
      {
        SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateProgress, 40 ) );
        Thread.Sleep( 5000 );
      }
      else
      {
        SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateProgress, 40 ) );
        errorCode = _configUpdate.DoUpdate();
        if( errorCode != ErrorCode.NoError )
        {
          SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateTerminated, errorCode ) );
          _logger.LogError( $"DoUpdate,end,errorCode={errorCode}" );
          return errorCode;
        }
      }
      SaveUpdateStage( userId, 2, UpdateState.Done );
      #endregion

      #region Stage 3: Update Firmware
      Stage3:
      SaveUpdateStage( userId, 3, UpdateState.Start );
      if( _config.FirmwareUpdate.SimMode )
      {
        SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateProgress, 60 ) );
        Thread.Sleep( 5000 );
      }
      else
      {
        SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateProgress, 60 ) );
        SendUpdateProgress( new PipeMessage( PipeMessageType.FirmwareVersionInfo ) );
        _responseEvent.WaitOne();
        if( _firmwareVersionInfo != null )
        {
          var updateMatrix = _firmwareUpdate.CreateUpdateMatrix( _firmwareVersionInfo );
          if( updateMatrix.Count > 0 )
          {
            SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateFirmwareInit ) );
            _responseEvent.WaitOne();
            errorCode = _firmwareUpdate.DoUpdate( updateMatrix );
            if( errorCode != ErrorCode.NoError )
            {
              SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateTerminated, errorCode ) );
              _logger.LogError( $"DoUpdate,end,errorCode={errorCode}" );
              return errorCode;
            }
          }
        }
      }
      SaveUpdateStage( userId, 3, UpdateState.Done );
      #endregion

      #region Stage 4: Update Vision
      Stage4:
      SaveUpdateStage( userId, 4, UpdateState.Start );
      if( _config.VisionUpdate.SimMode )
      {
        SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateProgress, 80 ) );
        Thread.Sleep( 5000 );
      }
      else
      {
        SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateProgress, 80 ) );
        SendUpdateProgress( new PipeMessage( PipeMessageType.VisionVersionInfo ) );
        _responseEvent.WaitOne();
        errorCode = _visionUpdate.DoUpdate( _visionVersionInfo );
        if( errorCode != ErrorCode.NoError )
        {
          SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateTerminated, errorCode ) );
          _logger.LogError( $"DoUpdate,end,errorCode={errorCode}" );
          return errorCode;
        }
      }
      SaveUpdateStage( userId, 4, UpdateState.Done );
      #endregion

      Stage5:
      SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateProgress, 100 ) );
      Thread.Sleep( 1000 );
      SendUpdateProgress( new PipeMessage( PipeMessageType.UpdateDone ) );

      _logger.LogDebug( $"DoUpdate,end,errorCode={errorCode}" );
      return errorCode;
    }

    private ErrorCode DoRestart( int userId )
    {
      _logger.LogDebug( "DoRestart,start" );
      ErrorCode errorCode = ErrorCode.NoError;

      SendUpdateProgress( new PipeMessage( PipeMessageType.RestartInit ) );
      Thread.Sleep( 9000 );
      SendUpdateProgress( new PipeMessage( PipeMessageType.RestartStart ) );
      Thread.Sleep( 1000 );
      
      errorCode = FileManager.StopService( _config.SoftwareUpdate.FloServiceName );
      if( errorCode != ErrorCode.NoError )
      {
        SendUpdateProgress( new PipeMessage( PipeMessageType.RestartFailed ) );
        _logger.LogError( $"DoRestart,end,errorCode={errorCode}" );
        return errorCode;
      }

      #region Stage 5 : Update Software
      SaveUpdateStage( userId, 5, UpdateState.Start );
      if( !_config.SoftwareUpdate.SimMode )
      {
        errorCode = _softwareUpdate.DoUpdate();
        if( errorCode != ErrorCode.NoError )
        {
          SendUpdateProgress( new PipeMessage( PipeMessageType.RestartFailed ) );
          _logger.LogError( $"DoRestart,end,errorCode={errorCode}" );
          return errorCode;
        }
      }

      RemoveUpdateStage();
      #endregion

      errorCode = FileManager.StartService( _config.SoftwareUpdate.FloServiceName );
      if( errorCode != ErrorCode.NoError )
      {
        SendUpdateProgress( new PipeMessage( PipeMessageType.RestartFailed ) );
        _logger.LogError( $"DoRestart,end,errorCode={errorCode}" );
        return errorCode;
      }

      errorCode = FileManager.StopService( _config.SoftwareUpdate.UpdaterServiceName );
      if( errorCode != ErrorCode.NoError )
      {
        SendUpdateProgress( new PipeMessage( PipeMessageType.RestartFailed ) );
        _logger.LogError( $"DoRestart,end,errorCode={errorCode}" );
        return errorCode;
      }

      return errorCode;
    }

    private ErrorCode Backup()
    {
      _logger.LogDebug( "Backup,start" );

      //Binary
      var errorCode = FileManager.CleanDirectory( _config.SoftwareUpdate.BackupPath );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"Backup,end,errorCode={errorCode}" );
        return errorCode;
      }
      
      errorCode = FileManager.CopyDirectory( _config.SoftwareUpdate.BinaryPath,
                                             _config.SoftwareUpdate.BackupPath,
                                             true,
                                             FileManager.NOT_EQUAL,
                                             new[] { "Database", "logs", "Backup" } );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"Backup,end,errorCode={errorCode}" );
        return errorCode;
      }
      
      //Default config
      errorCode = FileManager.CleanDirectory( _config.ConfigUpdate.BackupPath );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"Backup,end,errorCode={errorCode}" );
        return errorCode;
      }
      
      errorCode = FileManager.CopyDirectory( _config.ConfigUpdate.DefaultConfigFilePath,
                                             _config.ConfigUpdate.BackupPath,
                                             true );
      if( errorCode != ErrorCode.NoError )
      {
        _logger.LogError( $"Backup,end,errorCode={errorCode}" );
        return errorCode;
      }

      _logger.LogDebug( $"Backup,end,errorCode={errorCode}" );
      return errorCode;
    }

    private void SaveUpdateStage( int userId, int updateStage, UpdateState updateState )
    {
      try
      {
        var filePath = Path.Combine( _config.SoftwareUpdate.BinaryPath, UPDATE_STAGE_NAME );
        var stage = string.Concat( userId, " ", updateStage, " ", updateState );
        File.WriteAllText( filePath, stage );
      }
      catch( DirectoryNotFoundException )
      { }
    }

    private void RemoveUpdateStage()
    {
      var filePath = Path.Combine( _config.SoftwareUpdate.BinaryPath, UPDATE_STAGE_NAME );
      File.Delete( filePath );
    }

    private void SendUpdateProgress( PipeMessage pipeMessage )
    {
      _logger.LogDebug( $"SendUpdateProgress,message={JsonConvert.SerializeObject( pipeMessage, new StringEnumConverter() )}" );
      _pipeClient.SendCommand( pipeMessage );
    }

    private void OnPipeClosed( object sender, EventArgs e )
    {
      _logger.LogDebug( "Server closed" );
    }
  }
}

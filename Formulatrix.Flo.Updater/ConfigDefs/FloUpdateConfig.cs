﻿using Formulatrix.Flo.ConfigUpdater.ConfigDefs;
using Formulatrix.Flo.FirmwareUpdater.ConfigDefs;
using Formulatrix.Flo.SoftwareUpdater.ConfigDefs;
using Formulatrix.Flo.SystemUpdater.ConfigDefs;
using Formulatrix.Flo.VisionUpdater.ConfigDefs;

namespace Formulatrix.Flo.Updater.ConfigDefs
{
  public class FloUpdateConfig
  {
    public FloSystemUpdateConfig SystemUpdate { get; set; }
    public FloSoftwareUpdateConfig SoftwareUpdate { get; set; }
    public FloConfigUpdateConfig ConfigUpdate { get; set; }
    public FloFirmwareUpdateConfig FirmwareUpdate { get; set; }
    public FloVisionUpdateConfig VisionUpdate { get; set; }
  }
}

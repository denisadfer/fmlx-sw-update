﻿namespace Formulatrix.Flo.Updater.ConfigDefs
{
  public class ConfigItem
  {
    public string ConfigPath { get; set; }
    public string ConfigSection { get; set; }
    public object ConfigDataInstance { get; set; }

    public ConfigItem( string path, string section, object instance )
    {
      ConfigPath = path;
      ConfigSection = section;
      ConfigDataInstance = instance;
    }
  }
}

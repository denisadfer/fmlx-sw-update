﻿using System.Threading;
using Microsoft.Extensions.Logging;
using System.Reflection;

namespace Formulatrix.Flo.Updater
{
  static class Program
  {
    private static ILogger _logger;
    private static readonly AutoResetEvent TriggerClose = new AutoResetEvent( false );

    static void Main()
    {
      UpdateFactory.Init();

      _logger = UpdateFactory.Get<ILoggerFactory>().CreateLogger( nameof(Program) );
      _logger.LogInformation( $"FLO Software Updater version = " +
                              $"{Assembly.GetEntryAssembly()?.GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion}" );

      var executor = UpdateFactory.Get<UpdateExecutor>();
      executor.Init();

      TriggerClose.WaitOne();
    }
  }
}

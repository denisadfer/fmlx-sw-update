﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Formulatrix.Protocol;
using Formulatrix.Protocol.SocketCAN;
using Formulatrix.Flo.ConfigUpdater;
using Formulatrix.Flo.Device.Service.ConfigDefs;
using Formulatrix.Flo.FirmwareUpdater;
using Formulatrix.Flo.FirmwareUpdater.ConfigDefs;
using Formulatrix.Flo.Service.JsonDiffPatchs;
using Formulatrix.Flo.Service.Shell;
using Formulatrix.Flo.SoftwareUpdater;
using Formulatrix.Flo.Updater.ConfigDefs;
using Formulatrix.Flo.Updater.NamedPipe;
using Formulatrix.Flo.VisionUpdater;
using Formulatrix.Flo.VisionUpdater.ConfigDefs;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Log4Net;
using Formulatrix.Firmware.BootLoader;
using Microsoft.Extensions.Configuration;
using ConfigurationManager = System.Configuration.ConfigurationManager;

using Formulatrix.Flo.SystemUpdater;
using Formulatrix.Flo.SystemUpdater.ConfigDefs;

namespace Formulatrix.Flo.Updater
{
  public static class UpdateFactory
  {
    private static FloUpdateConfig _config;
    private static SystemUpdateService _systemUpdateService;
    private static DeviceServiceConfig _deviceServiceConfig;
    private static FirmwareMatrixConfig _firmwareMatrixConfig;
    private static VisionServiceConfig _visionServiceConfig;

    private static ConfigUpdateService _configUpdateService;
    private static SoftwareUpdateService _softwareUpdateService;
    private static FirmwareUpdateService _firmwareUpdateService;
    private static VisionUpdateService _visionUpdateService;
    private static JsonDiffPatchService _jsonDiffPatchService;
    private static IService[] _shellServices;

    private static ILoggerFactory _loggerFactory;

    private static IDictionary<Type, object> _objectDict;

    private static PipeClient _pipeClient;

    private static UpdateExecutor _floUpdaterExecutor;

    private static List<(ushort Address, IFmlxDriver Driver, BootLoader.MCUPlatform McuPlatform, string DeviceName)> _devicesInformation;

    public static void Init()
    {
      CreateLogger();
      LoadConfigFiles();

      CreateDevicesInformation();

      CreatePipeClient();
      CreateServices();
      CreateUpdaterExecutor();

      RegisterObjects();
    }

    private static void CreateLogger()
    {
      _loggerFactory = new LoggerFactory()
       .AddLog4Net( ConfigurationManager.AppSettings["LogConfigPath"] );
    }

    private static void LoadConfigFiles()
    {
      string configPath = ConfigurationManager.AppSettings["ConfigPath"];
      string deviceServicePath = ConfigurationManager.AppSettings["DeviceServicePath"];
      string firmwareMatrixPath = ConfigurationManager.AppSettings["FirmwareMatrixPath"];
      string visionServicePath = ConfigurationManager.AppSettings["VisionServicePath"];
      Dictionary<Type, ConfigItem> configDictionary = new Dictionary<Type, ConfigItem>
      {
        { typeof( FloUpdateConfig ), new ConfigItem( configPath, "UpdaterConfig", null ) },
        { typeof( DeviceServiceConfig ), new ConfigItem( deviceServicePath, "DeviceService", null ) },
        { typeof( FirmwareMatrixConfig ), new ConfigItem( firmwareMatrixPath, "FirmwareMatrixConfig", null) },
        { typeof( VisionServiceConfig ), new ConfigItem( visionServicePath, "VisionService", null) }
      };

      IConfigurationRoot configuration = new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile(configPath, optional: false, reloadOnChange: false)
        .AddJsonFile(deviceServicePath, optional: false, reloadOnChange: false)
        .AddJsonFile( firmwareMatrixPath,false,false )
        .AddJsonFile( visionServicePath,false,false )
        .Build();

      foreach( KeyValuePair<Type, ConfigItem> configItem in configDictionary )
      {
        configItem.Value.ConfigDataInstance = configuration.GetSection( configItem.Value.ConfigSection ).Get( configItem.Key );
      }
      _config = (FloUpdateConfig)configDictionary[typeof( FloUpdateConfig )].ConfigDataInstance;
      _deviceServiceConfig = (DeviceServiceConfig) configDictionary[typeof(DeviceServiceConfig)].ConfigDataInstance;
      _firmwareMatrixConfig = (FirmwareMatrixConfig) configDictionary[typeof(FirmwareMatrixConfig)].ConfigDataInstance;
      _visionServiceConfig = (VisionServiceConfig) configDictionary[typeof(VisionServiceConfig)].ConfigDataInstance;
    }

    #region Drivers
    private static void CreateDevicesInformation()
    {
      _devicesInformation = new List<(ushort Address, IFmlxDriver Driver, BootLoader.MCUPlatform McuPlatform, string Device)>();
      
      CreatePipetteInformation();
      CreateSpannerInformation();
      CreateDeckInformation();
      CreatePressureRegulatorInformation();
      CreateBootLoaderInformation();
    }

    private static void CreatePipetteInformation()
    {
      var driverConfigs = _deviceServiceConfig.PipetteService.DriverConfigs;
      var mcuPlatform = _deviceServiceConfig.PipetteService.McuPlatform;
      var simMode = _config.FirmwareUpdate.SimMode;
      CreateDevicesInformation( driverConfigs, mcuPlatform, DeviceType.Pipette, simMode );
    }

    private static void CreateSpannerInformation()
    {
      var driverConfigs = _deviceServiceConfig.SpannerService.DriverConfigs;
      var mcuPlatform = _deviceServiceConfig.SpannerService.McuPlatform;
      var simMode = _config.FirmwareUpdate.SimMode;
      CreateDevicesInformation( driverConfigs, mcuPlatform, DeviceType.Spanner, simMode );
    }

    private static void CreateDeckInformation()
    {
      var driverConfig = _deviceServiceConfig.DeckService.DriverConfig;
      var mcuPlatform = _deviceServiceConfig.DeckService.McuPlatform;
      var simMode = _config.FirmwareUpdate.SimMode;
      CreateDevicesInformation( driverConfig, mcuPlatform, DeviceType.Deck, simMode );
    }

    private static void CreatePressureRegulatorInformation()
    {
      var driverConfigs = _deviceServiceConfig.PressureRegulatorService.DriverConfigs;
      var mcuPlatform = _deviceServiceConfig.PressureRegulatorService.McuPlatform;
      var simMode = _config.FirmwareUpdate.SimMode;
      CreateDevicesInformation( driverConfigs, mcuPlatform, DeviceType.PressureRegulator, simMode );
    }

    private static void CreateBootLoaderInformation()
    {
      var driverConfig = _deviceServiceConfig.BootLoaderService.DriverConfig;
      var simMode = _config.FirmwareUpdate.SimMode;
      CreateDevicesInformation( driverConfig, BootLoader.MCUPlatform.TexasInstrument, DeviceType.BootLoader, simMode );
    }

    private static void CreateDevicesInformation( DriverConfig[] driverConfigs, BootLoader.MCUPlatform mcuPlatform, DeviceType deviceType, bool simMode )
    {
      for( int i = 0; i < driverConfigs.Length; i++ )
      {
        if( _devicesInformation.All( x => x.Address != driverConfigs[i].Address ) )
        {
          var deviceName = string.Concat( deviceType, i );
          IFmlxDriver canDriver = null;
          if( !simMode )
          {
            if( driverConfigs[i].SocketCanVersion == SocketCanVersion.SocketCanV1 )
            {
              canDriver = new SocketCanFmlxDriver( driverConfigs[i].Address, driverConfigs[i].CanChannel, driverConfigs[i].ChannelType,
                _loggerFactory.CreateLogger<SocketCanFmlxDriver>() );
            }
            else
            {
              canDriver = new SocketCan2FmlxDriver( driverConfigs[i].Address, driverConfigs[i].CanChannel, driverConfigs[i].ChannelType,
                _loggerFactory.CreateLogger<SocketCan2FmlxDriver>() );
            }
          }
          if( canDriver != null )
          {
            canDriver.ReadTimeout = 2000;
          }
          _devicesInformation.Add( ( driverConfigs[i].Address, canDriver, mcuPlatform, deviceName ) );
        }
      }
    }

    private static void CreateDevicesInformation( DriverConfig driverConfig, BootLoader.MCUPlatform mcuPlatform, DeviceType deviceType, bool simMode )
    {
      IFmlxDriver canDriver = null;
      if( _devicesInformation.All( x => x.Address != driverConfig.Address ) )
      {
        if( !simMode )
        {
          if( driverConfig.SocketCanVersion == SocketCanVersion.SocketCanV1 )
          {
            canDriver = new SocketCanFmlxDriver( driverConfig.Address, driverConfig.CanChannel, driverConfig.ChannelType,
              _loggerFactory.CreateLogger<SocketCanFmlxDriver>() );
          }
          else
          {
            canDriver = new SocketCan2FmlxDriver( driverConfig.Address, driverConfig.CanChannel,
              driverConfig.ChannelType,
              _loggerFactory.CreateLogger<SocketCan2FmlxDriver>() );
          }
        }
      }
      if( canDriver != null )
      {
        canDriver.ReadTimeout = 2000;
      }
      _devicesInformation.Add( ( driverConfig.Address, canDriver, mcuPlatform, deviceType.ToString() ) );
    }
    #endregion

    private static void CreatePipeClient()
    {
      _pipeClient = new PipeClient( x => x.StartStringReaderAsync(), _loggerFactory.CreateLogger<PipeClient>() );
    }

    private static void CreateServices()
    {
      _jsonDiffPatchService = new JsonDiffPatchService( _loggerFactory.CreateLogger<JsonDiffPatchService>() );
      _shellServices = new IService[]
      {
        new SftpService( _visionServiceConfig.IpAddress, _visionServiceConfig.Username, _visionServiceConfig.Password ), 
        new SshService( _visionServiceConfig.IpAddress, _visionServiceConfig.Username, _visionServiceConfig.Password )
      };
      _systemUpdateService = new SystemUpdateService(_config.SystemUpdate, _loggerFactory.CreateLogger<SystemUpdateService>());
      _configUpdateService = new ConfigUpdateService( _config.ConfigUpdate, _jsonDiffPatchService, _loggerFactory.CreateLogger<ConfigUpdateService>() );
      _softwareUpdateService = new SoftwareUpdateService( _config.SoftwareUpdate, _loggerFactory.CreateLogger<SoftwareUpdateService>() );
      _firmwareUpdateService = new FirmwareUpdateService( _config.FirmwareUpdate, _devicesInformation, _firmwareMatrixConfig, _loggerFactory.CreateLogger<FirmwareUpdateService>() );
      _visionUpdateService = new VisionUpdateService( _config.VisionUpdate, _shellServices, _jsonDiffPatchService, _loggerFactory.CreateLogger<VisionUpdateService>() );
    }

    private static void CreateUpdaterExecutor()
    {
      _floUpdaterExecutor = new UpdateExecutor( _config, _pipeClient, _systemUpdateService, _configUpdateService, _softwareUpdateService, 
                                                _firmwareUpdateService, _visionUpdateService, _loggerFactory.CreateLogger<UpdateExecutor>() );
    }

    public static T Get<T>()
    {
      if( _objectDict.ContainsKey( typeof( T ) ) )
        return (T)_objectDict[typeof( T )];
      return default( T );
    }

    private static void RegisterObjects()
    {
      _objectDict = new Dictionary<Type, object>();
      _objectDict.Add( typeof( ILoggerFactory ), _loggerFactory );
      _objectDict.Add( typeof( UpdateExecutor ), _floUpdaterExecutor );
    }
  }
}

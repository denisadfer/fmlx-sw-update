﻿namespace Formulatrix.Flo.FirmwareUpdater
{
  public enum SocketCanVersion
  {
    SocketCanV1,
    SocketCanV2
  }
}
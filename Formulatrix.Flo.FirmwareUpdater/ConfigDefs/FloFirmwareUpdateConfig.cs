﻿using Newtonsoft.Json;

namespace Formulatrix.Flo.FirmwareUpdater.ConfigDefs
{
  public class FloFirmwareUpdateConfig
  {
    [JsonProperty( Required = Required.Always )]
    public string FileUpdatePath { get; set; }

    [JsonProperty( Required = Required.Always )]
    public bool HangFirmware { get; set; }

    [JsonProperty( Required = Required.Always )]
    public bool SimMode { get; set; }
  }
}

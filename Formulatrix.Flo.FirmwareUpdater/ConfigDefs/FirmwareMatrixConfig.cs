namespace Formulatrix.Flo.FirmwareUpdater.ConfigDefs
{
  public class FirmwareMatrixConfig
  {
    public FirmwareMatrix[] FirmwareMatrix { get; set; }
  }

  public class FirmwareMatrix
  {
    public DeviceType DeviceType { get; set; }
    public string FirmwareName { get; set; }
  }
}

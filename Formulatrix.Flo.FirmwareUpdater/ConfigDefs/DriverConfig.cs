﻿using Formulatrix.Flo.FirmwareUpdater;
using Formulatrix.Protocol.SocketCAN;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Formulatrix.Flo.Device.Service.ConfigDefs
{
  public class DriverConfig
  {
    [JsonProperty( Required = Required.Always )]
    public ushort Address { get; set; }

    [JsonProperty( Required = Required.Always )]
    public int CanChannel { get; set; }

    [JsonProperty( Required = Required.Always )]
    [JsonConverter( typeof( StringEnumConverter ) )]
    public ChannelType ChannelType { get; set; }
    
    [JsonProperty( Required = Required.Always )]
    [JsonConverter( typeof( StringEnumConverter ) )]
    public SocketCanVersion SocketCanVersion { get; set; }
  }
}
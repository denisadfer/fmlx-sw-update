﻿using Formulatrix.Flo.Device.Service.ConfigDefs;
using Formulatrix.Firmware.BootLoader;

namespace Formulatrix.Flo.FirmwareUpdater.ConfigDefs
{
  public class DeviceServiceConfig
  {
    public DeckServiceConfig DeckService { get; set; }
    
    public SpannerServiceConfig SpannerService { get; set; }
    
    public PipetteServiceConfig PipetteService { get; set; }
    
    public PressureRegulatorServiceConfig PressureRegulatorService { get; set; }
    
    public BootLoaderServiceConfig BootLoaderService { get; set; }
  }

  public class DeckServiceConfig
  {
    public DriverConfig DriverConfig { get; set; }
    public BootLoader.MCUPlatform McuPlatform { get; set; }
  }

  public class SpannerServiceConfig
  {
    public DriverConfig[] DriverConfigs { get; set; }
    public BootLoader.MCUPlatform McuPlatform { get; set; }
  }

  public class PipetteServiceConfig
  {
    public DriverConfig[] DriverConfigs { get; set; }
    public BootLoader.MCUPlatform McuPlatform { get; set; }
  }

  public class PressureRegulatorServiceConfig
  {
    public DriverConfig[] DriverConfigs { get; set; }
    public BootLoader.MCUPlatform McuPlatform { get; set; }
  }

  public class BootLoaderServiceConfig
  {
    public DriverConfig DriverConfig { get; set; }
  }
}

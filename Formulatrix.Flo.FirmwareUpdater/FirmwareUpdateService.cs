﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Formulatrix.Protocol;
using Formulatrix.Flo.FirmwareUpdater.ConfigDefs;
using Microsoft.Extensions.Logging;
using Formulatrix.Firmware.BootLoader;
using Microsoft.Extensions.Logging.Abstractions;


namespace Formulatrix.Flo.FirmwareUpdater
{
  public class FirmwareUpdateService
  {
    private readonly FloFirmwareUpdateConfig _config;
    private readonly FirmwareMatrixConfig _firmwareMatrixConfig;

    private readonly List<(ushort Address, IFmlxDriver Driver, BootLoader.MCUPlatform McuPlatform, string DeviceName)> _devicesInformation;
    private readonly ILogger _logger;

    private const ushort DEFAULT_BASE_COMMAND = 32512;
    private const string SIMULATION = "simulation";
    private const byte MAX_RETRY_UPDATE = 3;

    public FirmwareUpdateService( FloFirmwareUpdateConfig config,
                                   List<(ushort address, IFmlxDriver driver, BootLoader.MCUPlatform mcuPlatform, string deviceName)> devicesInformation,
                                   FirmwareMatrixConfig firmwareMatrixConfig,
                                   ILogger logger )
    {
      _config = config;
      _firmwareMatrixConfig = firmwareMatrixConfig;
      _logger = logger;

      _devicesInformation = devicesInformation;
    }

    /// <summary>
    /// Create Update Matrix
    /// </summary>
    /// <param name="firmwareVersionInfo"></param>
    /// <returns>Return update matrix -> key:device address, value:path file update</returns>
    public Dictionary<ushort, string> CreateUpdateMatrix( Dictionary<string, string> firmwareVersionInfo )
    {
      var updateDeckVersion = string.Empty;
      var updateSpannerVersion = string.Empty;
      var updatePipetteVersion = string.Empty;
      var updatePressureRegVersion = string.Empty;

      var deckFilePath = string.Empty;
      var spannerFilePath = string.Empty;
      var pipetteFilePath = string.Empty;
      var pressureRegFilePath = string.Empty;
      
      var updateMatrix = new Dictionary<ushort, string>();

      foreach( var file in Directory.EnumerateFiles( _config.FileUpdatePath ) )
      {
        var fileName = Path.GetFileNameWithoutExtension( file );
        var firmwareName = fileName.Split( '_' )[0];
        var firmwareVersion = fileName.Split( '_' )[1];

        var firmwareMatrix = Array.FindAll( _firmwareMatrixConfig.FirmwareMatrix, x => x.FirmwareName == firmwareName );
        foreach( var obj in firmwareMatrix )
        {
          switch( obj.DeviceType )
          {
            case DeviceType.Deck:
              updateDeckVersion = firmwareVersion;
              deckFilePath = file;
              break;
            case DeviceType.Spanner:
              updateSpannerVersion = firmwareVersion;
              spannerFilePath = file;
              break;
            case DeviceType.Pipette:
              updatePipetteVersion = firmwareVersion;
              pipetteFilePath = file;
              break;
            case DeviceType.PressureRegulator:
              updatePressureRegVersion = firmwareVersion;
              pressureRegFilePath = file;
              break;
          }
        }
      }

      foreach( var obj in firmwareVersionInfo )
      {
        if( obj.Key.StartsWith( DeviceType.Deck.ToString() ) )
        {
          if( IsNeedToUpdate( obj.Value, updateDeckVersion ) )
          {
            AddUpdateMatrix( updateMatrix, deckFilePath, obj.Key );
          }
        }
        else if( obj.Key.StartsWith( DeviceType.Spanner.ToString() ) )
        {
          if( IsNeedToUpdate( obj.Value, updateSpannerVersion ) )
          {
            AddUpdateMatrix( updateMatrix, spannerFilePath, obj.Key );
          }
        }
        else if( obj.Key.StartsWith( DeviceType.Pipette.ToString() ) )
        {
          if( IsNeedToUpdate( obj.Value, updatePipetteVersion ) )
          {
            AddUpdateMatrix( updateMatrix, pipetteFilePath, obj.Key );
          }
        }
        else if( obj.Key.StartsWith( DeviceType.PressureRegulator.ToString() ) )
        {
          if( IsNeedToUpdate( obj.Value, updatePressureRegVersion ) )
          {
            AddUpdateMatrix( updateMatrix, pressureRegFilePath, obj.Key );
          }
        }
      }

      return updateMatrix;
    }

    private static bool IsNeedToUpdate( string currentVersion, string updateVersion )
    {
      if( currentVersion.ToLower().Equals( SIMULATION ) || updateVersion.Equals( string.Empty ) )
        return false;
      
      var result = false;
      currentVersion = currentVersion.Split( ' ' )[0]; //in case there is "DBG" postfix
      var currentVersionSplit = currentVersion.Split( '.' );
      var updateVersionSplit = updateVersion.Split( ' ' )[0].Split( '.' ); //in case there is "DBG" postfix

      if( currentVersionSplit.Length != updateVersionSplit.Length )
        return false;
      
      for( int i = 0; i < updateVersionSplit.Length; i++ )
      {
        if( int.Parse( updateVersionSplit[i], CultureInfo.InvariantCulture ) > int.Parse( currentVersionSplit[i], CultureInfo.InvariantCulture ) )
        {
          result = true;
          break;
        }
      }

      return result;
    }

    private void AddUpdateMatrix( IDictionary<ushort, string> updateMatrix, string filePath, string deviceName )
    {
      var deviceInformation = _devicesInformation.FirstOrDefault( x => x.DeviceName == deviceName );
      if( !string.IsNullOrEmpty( deviceInformation.DeviceName ) && 
           deviceInformation.Driver != null )
      {
        updateMatrix.Add( deviceInformation.Address, filePath );
      }
    }

    public ErrorCode DoUpdate( Dictionary<ushort, string> updateMatrix )
    {
      var errorCode = ErrorCode.NoError;
      
      var bootLoaderInformation = _devicesInformation.FirstOrDefault( x => x.DeviceName == DeviceType.BootLoader.ToString() );
      var bootLoaderDriver = bootLoaderInformation.Driver;
      var bootLoaderAddress = bootLoaderInformation.Address;
      var bootLoaderController = new FmlxController( bootLoaderDriver, new NullLogger<FmlxController>() );

      foreach( var obj in updateMatrix )
      {
        var address = obj.Key;
        var fileUpdatePath = obj.Value;

        var deviceInformation = _devicesInformation.FirstOrDefault( x => x.Address == address );
        var deviceDriver = deviceInformation.Driver;
        var deviceAddress = deviceInformation.Address;
        var deviceMcuPlatform = deviceInformation.McuPlatform;
        var deviceController = new FmlxController( deviceDriver, new NullLogger<FmlxController>() );
        var deviceName = deviceInformation.DeviceName;

        _logger.LogDebug( $"DoUpdate,deviceName={deviceName},address={address},fileUpdate={Path.GetFileName( fileUpdatePath )},start" );

        var bootloader = new BootLoader( bootLoaderController,
                                         deviceController,
                                         bootLoaderAddress,
                                         deviceAddress,
                                         _logger,
                                         _config.HangFirmware ? BootLoader.ResetOption.Hang : BootLoader.ResetOption.Default,
                                         DEFAULT_BASE_COMMAND,
                                         deviceMcuPlatform );
        bootLoaderController.Connect();
        
        if( bootLoaderDriver != deviceDriver )
        {
          deviceController.Connect();
        }
        
        bootloader.Open();
        bootloader.ProgressUpdated += OnProgressUpdated;

        var result = Upgrade( bootloader, fileUpdatePath );
        if ( result )
        {
          _logger.LogDebug( $"DoUpdate,deviceName={deviceName},address={address},fileUpdate={Path.GetFileName( fileUpdatePath )},end,errorCode={errorCode}" );
        }
        else
        {
          errorCode = ErrorCode.UpdateFirmwareFailed;
          _logger.LogError( $"DoUpdate,deviceName={deviceName},address={address},fileUpdate={Path.GetFileName( fileUpdatePath )},end,errorCode={errorCode}" );
        }
        
        bootloader.Close();
        bootloader.ProgressUpdated -= OnProgressUpdated;
        bootLoaderController.Disconnect();
        deviceController.Disconnect();

        if( errorCode != ErrorCode.NoError )
        {
          break;
        }
      }

      return errorCode;
    }
    
    private bool Upgrade(BootLoader bootLoader, string filename )
    {
      _logger.LogInformation( $"Upgrade,filename={filename},start" );
      var result = true;

      var retry = 0;
      while( retry < MAX_RETRY_UPDATE )
      {
        try
        {
          bootLoader.UpgradeFirmware( filename );
          break;
        }
        catch( Exception ex )
        {
          _logger.LogError( $"msg={string.Concat( ex.Message, ex.InnerException )},stackTrace={ex.StackTrace}" );
          retry++;
          if( retry == MAX_RETRY_UPDATE )
            result = false;
        }
      }

      _logger.LogInformation( $"Upgrade,filename={filename},end,return={result}" );
      return result;
    }
    
    private void OnProgressUpdated( object sender, BootLoader.BootLoaderProgressedEventArgs e )
    {
      _logger.LogDebug( $"{e.Action.ToString()}: {e.Percentage}" );
    }
  }
}

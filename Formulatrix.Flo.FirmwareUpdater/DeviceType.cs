namespace Formulatrix.Flo.FirmwareUpdater
{
  public enum DeviceType
  {
    Deck,
    Spanner,
    Pipette,
    PressureRegulator,
    BootLoader
  }
}
